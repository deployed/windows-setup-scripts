@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set HOMEPATH="C:\Users\user"

set BACKUP_SCRIPT_PATH=%HOMEPATH%\backup.ps1
set BACKUP_SCRIPT_COPY=%BASEDIR%user-settings\backup.ps1

set CLOUD_BACKUP_SCRIPT_PATH=%HOMEPATH%\cloud_backup.ps1
set CLOUD_BACKUP_SCRIPT_COPY=%BASEDIR%user-settings\cloud_backup.ps1

set WGET_BOOKMARKS_SCRIPT_PATH=%HOMEPATH%\wget_bookmarks.sh
set WGET_BOOKMARKS_SCRIPT_COPY=%BASEDIR%user-settings\wget_bookmarks.sh

call:%~1
goto:eof

:func
  echo:
  echo Copying backup scripts now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if '%BACKUP_SCRIPT_PATH%' exists...
  if not exist %BACKUP_SCRIPT_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %BACKUP_SCRIPT_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying 'backup.ps1' script into '%HOMEPATH%'
    echo:
    timeout /t 2 /nobreak > nul
    copy /Y %BACKUP_SCRIPT_COPY% %HOMEPATH%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> 'backup.ps1' script already exists!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if '%CLOUD_BACKUP_SCRIPT_PATH%' exists...
  if not exist %CLOUD_BACKUP_SCRIPT_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %CLOUD_BACKUP_SCRIPT_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying 'cloud_backup.ps1' script into '%HOMEPATH%'
    echo:
    timeout /t 2 /nobreak > nul
    copy /Y %CLOUD_BACKUP_SCRIPT_COPY% %HOMEPATH%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> 'cloud_backup.ps1' script already exists!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if '%WGET_BOOKMARKS_SCRIPT_PATH%' exists...
  if not exist %WGET_BOOKMARKS_SCRIPT_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %WGET_BOOKMARKS_SCRIPT_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying 'wget_bookmarks.sh' script into '%HOMEPATH%'
    echo:
    timeout /t 2 /nobreak > nul
    copy /Y %WGET_BOOKMARKS_SCRIPT_COPY% %HOMEPATH%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> 'wget_bookmarks.sh' script already exists!
  )  
goto:eof