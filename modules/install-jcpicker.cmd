@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set HOMEPATH="C:\Users\user"

set JCPICKER_COPY=%BASEDIR%portable-apps\JCPicker-Portable.zip
set JCPICKER_PATH="%ROOTDIR%Program Files"\JCPicker-Portable\
set JCPICKER_PATH_PS='%ROOTDIR%Program Files\JCPicker-Portable\'

set JCPICKER_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\jcpicker.exe"
set JCPICKER_EXE="C:\Program Files\"JCPicker-Portable\jcpicker.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing Just Color Picker program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if Just Color Picker already exists...
   if not exist %JCPICKER_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> Just Color Picker not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying Just Color Picker into %JCPICKER_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    mkdir %JCPICKER_PATH%
    powershell Expand-Archive %JCPICKER_COPY% -DestinationPath %JCPICKER_PATH_PS%
    timeout /t 2 /nobreak > nul
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Just Color Picker is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding Just Color Picker to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %JCPICKER_START_MENU_PATH% (
    mklink %JCPICKER_START_MENU_PATH% %JCPICKER_EXE%
  ) else (
    echo ^> Just Color Picker is already in the start menu!
  )
goto:eof