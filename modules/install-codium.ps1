
$current_dir=$PWD.Path

$home_path="C:\Users\user"
$base_path="$current_dir\.."
$base_dir_name="windows-shell-scripts"

if ((get-item $base_path).Name -ne $base_dir_name) {
  $base_path="$current_dir"
}

$extensions_path="$home_path\scoop\apps\VSCodium\current\data\user-data\User\extensions.list"
$extensions_copy="$base_path\user-settings\AppData\Roaming\VSCodium\User\extensions.list"

Start-Sleep -Seconds 1.5
Write-Output "`nNow checking if VSCodium is installed...`n"
Start-Sleep -Seconds 1.5

if (Get-Command vscodium) {

  Write-Output "> VSCodium is already installed!`n"

} else {

  if (Get-Command scoop) {

    Write-Output "VSCodium does not exist!`n"
    Start-Sleep -Seconds 1.5
    Write-Output "> Installing VSCodium now...`n"
    scoop install vscodium
    Start-Sleep -Seconds 1.5
    Write-Output "VSCodium is now installed!"
    vscodium
  } else {

    Write-Output "Scoop does not exist!"
    Start-Sleep -Seconds 1.5
    Write-Output "VSCodium was not installed!"

  }

}

# Start-Sleep -Seconds 1.5
# Write-Output "Now checking if VSCodium extensions exist...`n"
# Start-Sleep -Seconds 1.5

# if (Get-Command vscodium) {

  # vscodium --list-extensions > $extensions_path

  # if (Compare-Object -ReferenceObject @((Get-Content $extensions_path) | Select-Object) -DifferenceObject (Get-Content $extensions_copy)) {

    # Write-Output "> VSCodium extensions were not found`n"
    # cat $extensions_copy | % { vscodium --install-extension $_ }
    # Start-Sleep -Seconds 1.5
    # Write-Output "`nFinished installing extensions!`n"

  # } else {

    # Write-Output "> VSCodium extensions are already installed!`n"
  # }

# } else {

  # Write-Output "VSCodium is not installed!`n"
  # Start-Sleep -Seconds 1.5
  # Write-Output "VSCodium extensions weren't installed!"

# }
