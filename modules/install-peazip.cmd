@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set PROGRAM_FILES_PATH_PS='%ROOTDIR%Program Files\'
set PEAZIP_COPY=%BASEDIR%portable-apps\PeaZip-Portable.zip
set PEAZIP_PATH="%ROOTDIR%Program Files\"PeaZip-Portable\

set PEAZIP_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\peazip.exe"
set PEAZIP_EXE="C:\Program Files\"PeaZip-Portable\peazip.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing PeaZip program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if PeaZip already exists...
   if not exist %PEAZIP_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> PeaZip was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying PeaZip into %PEAZIP_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    powershell Expand-Archive %PEAZIP_COPY% -DestinationPath %PROGRAM_FILES_PATH_PS%
    timeout /t 2 /nobreak > nul
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> PeaZip is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding PeaZip to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %PEAZIP_START_MENU_PATH% (
    mklink %PEAZIP_START_MENU_PATH% %PEAZIP_EXE%
  ) else (
    echo ^> PeaZip is already in the start menu!
  )
goto:eof