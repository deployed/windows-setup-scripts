
$current_dir=$PWD.Path

$home_path="C:\Users\user"
$base_path="$current_dir\.."
$base_dir_name="windows-shell-scripts"

if ((get-item $base_path).Name -ne $base_dir_name) {
  $base_path="$current_dir"
}

Start-Sleep -Seconds 1.5
Write-Output "`nNow checking if 'Git for Windows' is installed...`n"
Start-Sleep -Seconds 1.5

if (Get-Command git-bash) {

  Write-Output "> 'Git for Windows' is already installed!`n"

} else {

  if (Get-Command scoop) {

    Write-Output "'Git for Windows' does not exist!`n"
    Start-Sleep -Seconds 1.5
    Write-Output "> Installing 'Git for Windows' now...`n"
    scoop install git
    Start-Sleep -Seconds 1.5
    Write-Output "'Git for Windows' is now installed!"
    Start-Sleep -Seconds 1.5
    scoop bucket add extras
    
  } else {

    Write-Output "Scoop does not exist!"
    Start-Sleep -Seconds 1.5
    Write-Output "'Git for Windows' was not installed!"

  }

}
