@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set FLIPWHEEL_COPY=%BASEDIR%portable-apps\FlipWheel-Portable\
set FLIPWHEEL_PATH="%ROOTDIR%Program Files"\FlipWheel-Portable\

set FLIPWHEEL_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\FlipWheel.exe"
set FLIPWHEEL_EXE="C:\Program Files\"FlipWheel-Portable\FlipWheel.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing FlipWheel program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if FlipWheel already exists...
  if not exist %FLIPWHEEL_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> FlipWheel program not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying FlipWheel into %FLIPWHEEL_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    xcopy %FLIPWHEEL_COPY% %FLIPWHEEL_PATH% /s /f
    timeout /t 2 /nobreak > nul
    echo:
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> FlipWheel is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding FlipWheel to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %FLIPWHEEL_START_MENU_PATH% (
    mklink %FLIPWHEEL_START_MENU_PATH% %FLIPWHEEL_EXE%
  ) else (
    echo ^> FlipWheel is already in the start menu!
  )
goto:eof
