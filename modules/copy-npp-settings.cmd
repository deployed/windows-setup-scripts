@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set HOMEPATH="C:\Users\user"
set NPP_SETTINGS_PATH=%HOMEPATH%\AppData\Roaming\Notepad++\
set NPP_SETTINGS_COPY=%BASEDIR%user-settings\AppData\Roaming\Notepad++

call:%~1
goto:eof

:func
  echo:
  echo Copying Notepad++ settings now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if Notepad++ settings exists...
  if not exist %NPP_SETTINGS_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %NPP_SETTINGS_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying Notepad++ settings into '%NPP_SETTINGS_PATH%'
    echo:
    timeout /t 2 /nobreak > nul
    xcopy %NPP_SETTINGS_COPY% %NPP_SETTINGS_PATH% /s /f
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Notepad++ settings are already installed!
  )
goto:eof