@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set RUFUS_COPY=%BASEDIR%portable-apps\Rufus-Portable\
set RUFUS_PATH="%ROOTDIR%Program Files"\Rufus-Portable\

set RUFUS_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\rufus.exe"
set RUFUS_EXE="C:\Program Files\"Rufus-Portable\rufus.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing Rufus program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if Rufus already exists...
  if not exist %RUFUS_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> Rufus program not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying Rufus into %RUFUS_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    xcopy %RUFUS_COPY% %RUFUS_PATH% /s /f
    timeout /t 2 /nobreak > nul
    echo:
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Rufus is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding Rufus to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %RUFUS_START_MENU_PATH% (
    mklink %RUFUS_START_MENU_PATH% %RUFUS_EXE%
  ) else (
    echo ^> Rufus is already in the start menu!
  )
goto:eof
