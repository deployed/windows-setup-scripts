
$scoop="scoop"

Start-Sleep -Seconds 1.5
Write-Output "`nChecking if $scoop is installed...`n"
Start-Sleep -Seconds 1.5
if (Get-Command $scoop) {

  Write-Output "> $scoop is already installed!"

} else {
  Write-Output "$scoop does not exist!"
  Start-Sleep -Seconds 1.5
  Write-Output "> Installing $scoop now...`n"
  Start-Sleep -Seconds 1.5
  Set-ExecutionPolicy -ExecutionPolicy RemoteSigned
  Set-ExecutionPolicy RemoteSigned -Scope CurrentUser # Optional: Needed to run a remote script the first time
  irm get.scoop.sh | iex
  scoop bucket add extras
  scoop install 7zip
  scoop install wget
  Write-Output "$scoop is now installed!"
}
