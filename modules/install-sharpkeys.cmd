@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set SHARPKEYS_COPY=%BASEDIR%portable-apps\SharpKeys-Portable.zip
set SHARPKEYS_PATH="%ROOTDIR%Program Files"\SharpKeys-Portable\
set SHARPKEYS_PATH_SH='%ROOTDIR%Program Files\SharpKeys-Portable\'

set SHARPKEYS_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\SharpKeys.exe"
set SHARPKEYS_EXE="C:\Program Files\"SharpKeys-Portable\SharpKeys.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing SharpKeys program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if SharpKeys already exists...
  if not exist %SHARPKEYS_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> SharpKeys was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying SharpKeys into %SHARPKEYS_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    mkdir %SHARPKEYS_PATH%
    powershell Expand-Archive %SHARPKEYS_COPY% -DestinationPath %SHARPKEYS_PATH_SH%
    timeout /t 2 /nobreak > nul
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> SharpKeys is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding SharpKeys to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %SHARPKEYS_START_MENU_PATH% (
    mklink %SHARPKEYS_START_MENU_PATH% %SHARPKEYS_EXE%
  ) else (
    echo ^> SharpKeys is already in the start menu!
  )
goto:eof