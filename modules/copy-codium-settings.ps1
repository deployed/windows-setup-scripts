
$current_dir=$PWD.Path

$home_path="C:\Users\user"
$base_path="$current_dir\.."
$base_dir_name="windows-shell-scripts"

if ((get-item $base_path).Name -ne $base_dir_name) {
  $base_path="$current_dir"
}

$codium_user_path="$home_path\scoop\apps\vscodium\current\data\user-data\User\"
$codium_user_prefs="$base_path\user-settings\AppData\Roaming\VSCodium\User\*"

$codium_keybindings_path="$home_path\scoop\apps\vscodium\current\data\user-data\User\keybindings.json"
$codium_settings_path="$home_path\scoop\apps\vscodium\current\data\user-data\User\settings.json"
$codium_settings_copy="$base_path\user-settings\AppData\Roaming\VSCodium\User\settings.json"

Start-Sleep -Seconds 1.5
Write-Output "Checking to see if VSCodium settings exist...`n"
Start-Sleep -Seconds 1.5

if ((Test-Path $codium_keybindings_path) -and (Test-Path $codium_settings_path)) {

  if (Compare-Object -ReferenceObject (Get-Content $codium_settings_path) -DifferenceObject (Get-Content $codium_settings_copy)) {

    Write-Output "VSCodium settings were not found!`n"
    Start-Sleep -Seconds 1.5
    Write-Output "> Copying files now...`n"
    Copy-Item -Path $codium_user_prefs -Destination $codium_user_path -Recurse
    Start-Sleep -Seconds 1.5
    Write-Output "Done!"

  } else {

    Write-Output "> VSCodium settings are already installed!`n"

  }

} else {

  Write-Output "> VSCodium settings were not found!"
  Start-Sleep -Seconds 1.5
  Write-Output "> Copying files now..."
  Copy-Item -Path $codium_user_prefs -Destination $codium_user_path -Recurse
  Start-Sleep -Seconds 1.5
  Write-Output "> Done!`n"

}
