@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set PROGRAM_FILES_PATH_PS='%ROOTDIR%Program Files\'

set NPP_COPY=%BASEDIR%portable-apps\NPP-Portable-x86.zip
set NPP_PATH="%ROOTDIR%Program Files (x86)"\NPP-Portable-x86\
set NPP_PATH_PS='"%ROOTDIR%Program Files (x86)"\NPP-Portable-x86\'

set NPP_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\notepad++.exe"
set NPP_EXE="C:\Program Files (x86)\"NPP-Portable-x86\notepad++.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing Notepad++ program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if Notepad++ already exists...
  if not exist %NPP_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> Notepad++ program not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying Notepad++ program into %NPP_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    mkdir %NPP_PATH%
    powershell Expand-Archive %NPP_COPY% -DestinationPath %NPP_PATH_PS%
    REM powershell 7z x %NPP_COPY% -o%NPP_PATH_PS% -r
    timeout /t 2 /nobreak > nul
    echo:
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Notepad++ is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding Notepad++ to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %NPP_START_MENU_PATH% (
    mklink %NPP_START_MENU_PATH% %NPP_EXE%
  ) else (
    echo ^> Notepad++ is already in the start menu!
  )
goto:eof
