@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set HOMEPATH="C:\Users\user"

set PROGRAM_FILES_PATH_PS='%ROOTDIR%Program Files\'
set COPYQ_COPY=%BASEDIR%portable-apps\CopyQ-Portable.zip
set COPYQ_PATH="%ROOTDIR%Program Files\"CopyQ-Portable\

set COPYQ_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\copyq.exe"
set COPYQ_EXE="C:\Program Files\"CopyQ-Portable\copyq.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing CopyQ program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if CopyQ already exists...
   if not exist %COPYQ_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> CopyQ program was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying CopyQ into %COPYQ_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    powershell Expand-Archive %COPYQ_COPY% -DestinationPath %PROGRAM_FILES_PATH_PS%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
    timeout /t 2 /nobreak > nul
    echo Creating a CopyQ task to execute program at logon...
    timeout /t 2 /nobreak > nul
    powershell $user= 'user'; $copyqPath= 'C:\Program Files\CopyQ-Portable\copyq.exe'; $trigger= "New-ScheduledTaskTrigger -AtLogon"; $action= New-ScheduledTaskAction -Execute $copyqPath; $settings= New-ScheduledTaskSettingsSet  -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit 0; Register-ScheduledTask -TaskName "Start_CopyQ" -Trigger $trigger -User $user -Action $action -Settings $settings -RunLevel Highest -Force
    timeout /t 2 /nobreak > nul
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> CopyQ is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding CopyQ to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %COPYQ_START_MENU_PATH% (
    mklink %COPYQ_START_MENU_PATH% %COPYQ_EXE%
  ) else (
    echo ^> CopyQ is already in the start menu!
  )
goto:eof
