# Windows Grunt Scripts

These Windows grunt scripts assist with doing your grunt work after installing Windows...

They copy over essential files and settings, as well as help install important portable applications.

The scripts are extremely useful when clean installing (or reinstalling) Windows. This repo includes the runner and module scripts.

I wrote these scripts for use on Windows 10, but they can be easily altered to suit your use cases.

## Getting Started

Before running the Powershell scripts, you may need to give your Windows machine permission to execute such scripts.

Open up Windows Powershell (without admin privledges) and enter in the command:

```
set-executionpolicy remotesigned -scope currentuser
```

*(answer `[A] Yes to All` when prompted to change the execution policy)*

## How To Use The Powershell Scripts

1) Clone the `windows-grunt-scripts\` repo into `Downloads\`
2) Open up Powershell (without admin privledges), then `cd` into `Downloads\windows-grunt-scripts\`
3) Within Powershell, run the grunt setup script by typing `.\run.ps1` and hitting `Enter`

## Running The Ungoogled-Chromium Installer Script

Now we need a fast, reliable, and privacy-centric browser that will replace Microsoft Edge.

[Ungoogled-Chromium](https://github.com/ungoogled-software/ungoogled-chromium) will do just fine.

Here's how to run the grunt script to install Ungoogled-Chromium...

1) Open up Powershell (with admin privledges), then `cd` into `C:\Users\YOUR_USERNAME\Downloads\windows-grunt-scripts\`
2) Within Powershell, run the ungoogled-chromium installer script by typing `.\modules\install-chromium.ps1` and hitting `Enter`

Next you'll want to install the Chromium Web Store. Follow these steps:

1) Enter in `chrome://flags` in the Chromium address bar and search for the `#extension-mime-request-handling` flag and set it to `Always prompt for install`.
2) Next, download the .crx from [Releases](https://github.com/NeverDecaf/chromium-web-store/releases/), where you'll be prompted to install the extension.

> Hooray! Now you can install Chrome extenstions like [OneTab](https://www.one-tab.com/), [BitWarden](https://bitwarden.com/), and [Ublock Origin](https://ublockorigin.com/).

Annnd... let's add the Google search to Ungoogled-Chromium (since the browser doesn't ship with it by default):

1) Go to `chrome://settings` in the address bar and click on `Manage search engines and site search`
2) Scroll down to `Site Search` and click on `Add`
3) Enter in the following data into the form field:
    - **Name**: `Google`
    - **Shortcut**: `google.com`
    - **URL with %s in place of query**: `https://www.google.com/search?q=%s`
    - **Suggested URL with %s in place of query**: `https://www.google.com/complete/search?client=chrome&q=%s`

## How To Use The Command Prompt Scripts

1) Open the Command Prompt (w/ Admin Privledges), then `cd` into `Downloads\windows-grunt-scripts\`
2) Inside Command Prompt, run the setup script by typing `.\setup.cmd` and hitting `Enter`

## What's Next?

After running the Windows grunt scripts, I wrap up the process by installing essential, privacy-oriented applications, which include:

- [Nextcloud](https://nextcloud.com/install/)
- [StandardNotes](https://standardnotes.com/download)
- [ProtonVPN](https://protonvpn.com/download)

Last but not least, I connect my version control system (i.e. git) with my remote development platform (i.e. gitlab, github) via SSH using these steps:

1) Open up [Git Bash](https://gitforwindows.org/)
2) Enter the command `ssh-keygen -t ed25519 -C "<comment>"` to generate a SSH key.
3) Next, enter the command `cat ~/.ssh/id_ed25519.pub | clip` to copy the contents of your public key file.
4) Depending on your remote platform, you can copy this public key into your development account to securely communicate with Git.

## How The Command Prompt Grunt Scripts Work

*(useful to read if you wanna learn more about them)*

After the `setup.cmd` script is executed, the user will be prompted to `continue` or `exit` the setup.

If the user opts to `continue`, the `setup.cmd` script will invoke each of the module scripts synchronously (until there are no more module scripts left to execute).

Each module is invoked within `setup.cmd` like so:

```
call %CURDIR%modules\install-sharpkeys.cmd func
call %CURDIR%modules\install-npp.cmd func
call %CURDIR%modules\install-revo.cmd func
call %CURDIR%modules\install-rufus.cmd func
call %CURDIR%modules\install-peazip.cmd func
```

Notice how each module includes a descriptive file name to indicate its specific role.

Each module is invoked using the `call` keyword, followed by the path to the module script.

`func` is the name of the function that is being 'called' within the module script.

## What do the Command Prompt module scripts do?

The install-peazip.cmd module script for example...

* First checks if the PeaZip application is already installed in the `Program Files\` directory.
* If the PeaZip application is not already installed, the module script unzips the file containing the peazip application (using the `powershell` command) and copies it to the `Program Files\` directory.

Other modules scripts utilize helpful commands like `xcopy` to copy over program files and settings, as well as the `powershell` command to tell the `Task Scheduler` application to perform scheduled tasks at logon.
