$usb1DriveLetter = (Get-Volume -FileSystemLabel "VERB128").DriveLetter

$usb2DriveLetter = (Get-Volume -FileSystemLabel "VERB128D").DriveLetter

$usb1Path = "${usb1DriveLetter}:\"
$usb2Path = "${usb2DriveLetter}:\"

$user = $env:username
$userPath = "C:\Users\${user}"

$cloudName = "Nextcloud"
$cloudPath = "C:\Users\${user}\Nextcloud"
$cloudWildcardPath = "*\Nextcloud\*"

if ( $Args.count -eq 0 ) {
  echo "No argument supplied, please specify a folder in your home directory."
  return
}

echo "`nChecking if your USBs are plugged in..."
Start-Sleep -Seconds 2

if ((Test-Path $usb1Path) -and (Test-Path  $usb2Path)) {
  echo "> Hooray! Your USBs were found.`n"
  Start-Sleep -Seconds 2
  echo "Now checking if your cloud drive exists..."

  if ((Test-Path $cloudPath)) {
    echo "> Yes! Your cloud drive was found.`n"

    $numOfArgs = $Args.count

    for (($i = 0); $i -lt $numOfArgs; $i++) {
      Start-Sleep -Seconds 2
      $folder = $Args[$i]
      echo "Backing up '${folder}' now...`n"

      $folderPath = Get-ChildItem $userPath -Recurse -Filter $folder | Where {$_.FullName -notlike $cloudWildcardPath } | select -expand FullName

      $usb1FolderPath = Get-ChildItem $usb1Path -Recurse -Filter $folder | select -expand FullName

      $usb2FolderPath = Get-ChildItem $usb2Path -Recurse -Filter $folder | select -expand FullName

      $cloudFolderPath = Get-ChildItem $cloudPath -Recurse -Filter $folder | select -expand FullName

      echo "> Folder path: '${folderPath}'`n"

      if ((Test-Path $folderPath)) {
        
        if ((Test-Path $usb1FolderPath) -and (Test-Path $usb2FolderPath)) {
          
          if ((Test-Path $cloudFolderPath)) {
            
            $usb1ParentPath = $usb1FolderPath -Replace "${folder}", ""
            $usb2ParentPath = $usb2FolderPath -Replace "${folder}", ""
            $cloudParentPath = $cloudFolderPath -Replace "${folder}", ""
            
            Remove-Item $usb1FolderPath -Recurse -Force
            Copy-Item $folderPath -Destination $usb1ParentPath -Recurse -Force

            Remove-Item $usb2FolderPath -Recurse -Force
            Copy-Item $folderPath -Destination $usb2ParentPath -Recurse -Force
            
            Remove-Item $cloudFolderPath -Recurse -Force
            robocopy $folderPath $cloudFolderPath /E /XD .git
            
          } else {
            echo "** '${folder}' does not exist in your cloud drive.**"
            Start-Sleep -Seconds 2
            echo "> '${folder}' not copied..."
            Start-Sleep -Seconds 2
            echo "> Please make sure the '${folder}' directory exists in your cloud drive, then rerun this script.`n"
          }
          
        } else {
          echo "** '${folder}' does not exist in your USB drives. **"
          Start-Sleep -Seconds 2
          echo "> '${folder}' not copied..."
          Start-Sleep -Seconds 2
          echo "> Please make sure the '${folder}' directory exists in your USB drives, then rerun this script.`n"
        }
        
      } else {
        echo "**'${folder}' was not found in your home directory.**"
        Start-Sleep -Seconds 2
        echo "> '${folder}' not copied..."
        Start-Sleep -Seconds 2
        echo "> Please make sure '${folder}' exists in your home directory, then rerun this script.`n"
      }

    }

    echo "`nThe backup script has finished!"
    Start-Sleep -Seconds 2
    echo "Exiting script now...`n"
    return


  } else {
    echo "Uh-oh! Your cloud drive could not be found."
    Start-Sleep -Seconds 2
    echo -e "> Please activate your cloud drive directory and run this script again!"
  }

} else {
  echo "Whoops! Your USBs aren't plugged in.`n"
  Start-Sleep -Seconds 2
  echo "> Please plug in your USBs and run this script again!"
}