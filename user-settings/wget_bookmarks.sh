
BOOKMARKS_DIR=~/Nextcloud/Documents/bookmarks
HTML_BOOKMARKS_DIR=$BOOKMARKS_DIR/html
WEBPAGES_DIR=$BOOKMARKS_DIR/webpages
WEBPAGES=$BOOKMARKS_DIR/webpages/*

if ! command -v wget; then
  echo -e "'wget' was not found in your system."
  sleep 2s
  echo -e "Please install 'wget', then rerun this script."
  return
fi

echo -e "\nNow running the 'wget_bookmarks.sh' script."

sleep 2s
echo -e "\nChecking if your 'Bookmarks' directory exists..."
sleep 2s

if [ -d $bookmarks_dir ]; then

  echo -e "> Hooray! Your 'Bookmarks' directory was found."
  sleep 2s
  echo -e "\nNow checking for the most recent bookmark.html file..."
  sleep 2s

  most_recent_bookmark=$(ls $HTML_BOOKMARKS_DIR -Art | tail -n 1)
  most_recent_bookmark_path=$HTML_BOOKMARKS_DIR/$most_recent_bookmark

  if [ -f $most_recent_bookmark_path ]; then

    echo -e "> '$most_recent_bookmark file' was found!"
    sleep 2s
    echo -e "> Now using 'wget' to store all bookmarks into the 'webpages' directory."
    sleep 2s

    rm -rf $WEBPAGES_DIR
    mkdir $WEBPAGES_DIR
    wget --user-agent=Epiphany --continue --no-clobber --tries=3 --read-timeout=10 --no-parent --convert-links --force-html --input-file=$most_recent_bookmark_path --directory-prefix=$WEBPAGES_DIR

    for file in $WEBPAGES; do
      new_file=$(printf %s/ "$file" | tr "?" "_")
      mv -- "$file" "${new_file%/}"
    done

  else
    echo -e "\nUh-oh! Bookmark files were not found in the 'bookmarks' directory.'"
    sleep 2s
    echo -e "> Please save at least one .html bookmark file in this directory, then rerun this script.\n"
  fi

else
  echo -e "\nWhoops! Your bookmarks directory was not found!"
  sleep 2s
  echo -e "> Please double-check this script to make sure the file path is correct for the 'Bookmarks' directory, then rerun this script\n"
fi
