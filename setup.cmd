@echo off

pushd \
set ROOTDIR=%CD%
popd
set CURDIR=%~dp0

echo: & :: new line
echo Welcome to your setup script!
timeout /t 1 /nobreak > nul & :: sleep for 1 second
choice /m "> Do you want to continue?"

if %errorlevel% == 1 (
  timeout /t 2 /nobreak > nul
  REM call %CURDIR%modules\install-git-bash.cmd func
  REM timeout /t 2 /nobreak > nul
  call %CURDIR%modules\install-breezily.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\install-copyq.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\install-sharpkeys.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\install-npp.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\install-flipwheel.cmd func
  timeout /t 2 /nobreak > nul  
  call %CURDIR%modules\install-rufus.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\install-peazip.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\install-jcpicker.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\copy-git-bash-settings.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\copy-backup-scripts.cmd func
  timeout /t 2 /nobreak > nul
  call %CURDIR%modules\copy-npp-settings.cmd func
  echo:
  echo The setup script is complete!
  echo:
  echo Goodbye...
)
if %errorlevel% == 2 (
  timeout /t 2 /nobreak > nul
  echo:
  echo Now exiting the setup script.
  timeout /t 1 /nobreak > nul
  echo Goodbye...
)
